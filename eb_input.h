
/**
 * This module serves as a means to acquire input from any FILE without
 * having to used a fixed-length currently-allocated buffer.
 *
 * @author Daniel Rosenwasser
 */

#ifndef __ENDLESS_BUFFER_IO_
#define __ENDLESS_BUFFER_IO_

#include <stdio.h>

/**
 * Reads from a given file pointer and returns a string that reads a full
 * line of input. All characters up to and including the trailing newline
 * are returned as a null-terminated string.
 *
 * @warning Side effects to beware of is that this function dynamically 
 * allocates the string. In other words, freeing the returned string is the
 * responsibilityof the caller, and not this function. The string is allocated
 * using the malloc family.
 *
 * @param fs a pointer to the file stream from which this function will read.
 *
 * @returns NULL if fs has encountered its end-of-file, or
 *          a null-terminated string of indeterminate length otherwise
 */
char* eb_read_line(FILE* fs);

#endif
