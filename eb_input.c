
/**
 * @author Daniel Rosenwasser
 */

#include "eb_input.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define BUFFER_INC_SIZE 256

char* eb_read_line(FILE* fs)
{
    char* buffer = NULL;

    // return NULL if at EOF
    if ( feof(fs) )
        return buffer;

    size_t buff_size = 0;
    size_t count = 0;
    int ch;

    // loop until we get an EOF
    while ( (ch = fgetc(fs)) != EOF )
    {
        // need to expand if we've reached
        // the end of the buffer
        if (buff_size == count)
        {
            buff_size += BUFFER_INC_SIZE;
            buffer = realloc(buffer, buff_size);
        }

        buffer[count] = ch;
        count++;

        if (ch == '\n')
            break;
    }

    // need to check for whether we
    // need to expand one more time for
    // the trailing NUL byte
    if (buff_size == count)
    {
        buff_size += 1;
        buffer = realloc(buffer, buff_size);
    }

    buffer[count] = '\0';
    
    return buffer;
}




