
/**
 * @author Daniel Rosenwasser
 * @author Matthew Newton
 */

#include "eb_input.h"
#include "exec_pipe_chain.h"
#include "tokenizer.h"
#include "strsupp.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <assert.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
    char* input = NULL;
    char** pipe_tokens = NULL;
    unsigned int num_pipe_tokens = 0;
    
    int child_status;
    
    while (!feof(stdin))
    {
        fputs("\n? ", stdout);
        fflush(stdout);
        input = eb_read_line(stdin);
        
        // test whether the string is empty without mutating it
        // (i.e. if all characters are whitespace, strlen => 0
        if (strlen(lstrip(input)) == 0)
            continue;
        
        fprintf(stdout, "%s", input);
        fflush(stdout);
        
        num_pipe_tokens = tokenize(input, PIPE_SEPARATOR, &pipe_tokens);
        
        exec_pipe_chain(pipe_tokens);
        
        for (int i = 0; i < num_pipe_tokens; i++)
        {
            wait(&child_status);
        }
        
        free(pipe_tokens);
        free(input);
        pipe_tokens = NULL;
        input = NULL;
    }

    fputs("\n", stdout);
    return 0;
}

