
/**
 * @author Daniel Rosenwasser
 */

#include "strsupp.h"

#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <ctype.h>

char* rstrip(char* str)
{
    size_t n = strlen(str);

    for (size_t i = 0; i < n; i++)
    {
        if ( isspace(str[(n-1)-i]) )
            str[(n-1)-i] = '\0';
        else
            break;
        
    }
        
    return str;
}

char* lstrip(const char* str)
{
    const char* s;
    
    for (s = str; *s != '\0'; s++)
    {
        if ( !isspace(*s) )
            break;
    }

    return (char*)s;
}

/*
inline char* strip(char* str)
{
    return lstrip(rstrip(str));
}*/


