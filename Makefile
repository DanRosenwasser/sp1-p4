#
# Created by makemake (Ubuntu Feb 17 2012) on Sun Feb 26 01:30:21 2012
#

#
# Definitions
#

.SUFFIXES:
.SUFFIXES:	.a .o .c .C .cpp .s .S
.c.o:
		$(COMPILE.c) $<
.C.o:
		$(COMPILE.cc) $<
.cpp.o:
		$(COMPILE.cc) $<
.S.s:
		$(CPP) -o $*.s $<
.s.o:
		$(COMPILE.s) -o $@ $<
.c.a:
		$(COMPILE.c) -o $% $<
		$(AR) $(ARFLAGS) $@ $%
		$(RM) $%
.C.a:
		$(COMPILE.cc) -o $% $<
		$(AR) $(ARFLAGS) $@ $%
		$(RM) $%
.cpp.a:
		$(COMPILE.cc) -o $% $<
		$(AR) $(ARFLAGS) $@ $%
		$(RM) $%

AS =		as
CC =		cc
CXX =		c++

RM = rm -f
AR = ar
LINK.c = $(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS)
LINK.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)
COMPILE.s = $(AS) $(ASFLAGS)
COMPILE.c = $(CC) $(CFLAGS) $(CPPFLAGS) -c
COMPILE.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) -c
CPP = $(CPP) $(CPPFLAGS)
########## Flags from header.mak

AS =		gas
CC =		gcc
CFLAGS =	-ggdb -std=c99

########## End of flags from header.mak


CPP_FILES =	
C_FILES =	eb_input.c exec_pipe_chain.c mysh.c strsupp.c tokenizer.c
PS_FILES =	
S_FILES =	
H_FILES =	eb_input.h exec_pipe_chain.h strsupp.h tokenizer.h
SOURCEFILES =	$(H_FILES) $(CPP_FILES) $(C_FILES) $(S_FILES)
.PRECIOUS:	$(SOURCEFILES)
OBJFILES =	eb_input.o exec_pipe_chain.o strsupp.o tokenizer.o 

#
# Main targets
#

all:	mysh 

mysh:	mysh.o $(OBJFILES)
	$(CC) $(CFLAGS) -o mysh mysh.o $(OBJFILES) $(CLIBFLAGS)

#
# Dependencies
#

eb_input.o:	eb_input.h
exec_pipe_chain.o:	exec_pipe_chain.h
mysh.o:	eb_input.h exec_pipe_chain.h strsupp.h tokenizer.h
strsupp.o:	strsupp.h
tokenizer.o:	tokenizer.h

#
# Housekeeping
#

Archive:	archive.tgz

archive.tgz:	$(SOURCEFILES) Makefile
	tar cf - $(SOURCEFILES) Makefile | gzip > archive.tgz

clean:
	-/bin/rm -f $(OBJFILES) mysh.o core

realclean:        clean
	-/bin/rm -f mysh 
