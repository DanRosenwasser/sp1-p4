
/**
 * A module to provide some supplements to the standard C string library.
 *
 * @author Daniel Rosenwasser
 */

#ifndef __STRING_SUPPLEMENT_
#define __STRING_SUPPLEMENT_

/**
 * char* rstrip - null terminates whitespace characters from the end of a string
 *
 * @param str a null-terminated string of characters
 *
 * @returns: The given string str.
 *
 * This function works from the end of str and sets all whitespace
 * characters to '\0' until the first non-whitespace character is encountered. A
 * character is determined to be whitespace by the isspace function of string.h
 *
 * Note that the parameter str is guaranteed to be modified if it has trailing
 * whitespace at its end!
 */
char* rstrip(char* str);

/**
 * char* lstrip - returns a pointer to the first non-whitespace character of str
 *
 * @param str a null-terminated string of characters
 *
 * @returns: a pointer to the first non-whitespace character of str
 *
 * This function works from the front of str and reads until the first
 * non-whitespace character is encountered. A character is determined to be
 * whitespace by the isspace function of string. hIt then returns a pointer to
 * this portion of the str. str is not modified, but the returned value has any
 * const-ness cast away from it.
 * 
 * Note that this function still points to the contents of str, and as such,
 * any modification to the string returned will ensue in a modification to str!
 */
char* lstrip(const char* str);

/**
 * Returns the result of lstrip applied to the result of rstrip applied
 * to the given argument. In other words,
 * 
 *      strip(str)
 *
 * is equivalent to
 *
 *      lstrip(rstrip(str));
 */
//extern char* strip(char* str);

#endif
