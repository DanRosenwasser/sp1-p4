/**
 * @author Daniel Rosenwasser
 * @author Matthew Newton
 */

#include "exec_pipe_chain.h"
#include "tokenizer.h"

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#define NO_REDIRECT -5

#define READ_REDIR  0
#define WRITE_REDIR 1

int find_redirects(char **, int, int *);
void remove_redirects(char ***, char ***, int, int, int);

void exec_pipe_chain(char** pipe_tokens)
{
    int pfd[2];
    
    int last_input = STDIN_FILENO;
    
    for (size_t tok_num = 0; pipe_tokens[tok_num] != NULL; tok_num++)
    {
        if (pipe_tokens[tok_num+1] == NULL)
        {
            pfd[PIPE_WRITE_END] = STDOUT_FILENO;
        }
        else
        {
            if (pipe(pfd) == -1)
            {
                _exit(1);
            }
        }
        
        switch (fork())
        {
        case -1:
            //perror("fork");
            _exit(1);
            break;
        case 0:
            // if this is not the last token, then we just close the read end 
            // of the pipe since we don't need it in this process
            if (pipe_tokens[tok_num+1] != NULL)
            {
                if (close(pfd[PIPE_READ_END]) == -1)
                {
                    _exit(1);
                }
            }
            
            // if this is not the last token, replace stdout with a pipe
            if (pipe_tokens[tok_num+1] != NULL && pfd[PIPE_WRITE_END] != STDOUT_FILENO)
            {
                if (dup2(pfd[PIPE_WRITE_END], STDOUT_FILENO) == -1)
                {
                    _exit(1);
                }
                if (close(pfd[PIPE_WRITE_END]) == -1)
                {
                    _exit(1);
                }
            }
            
            // if this is not the first token, then replace stdin with a pipe
            if (last_input != STDIN_FILENO)
            {
                if (dup2(last_input, STDIN_FILENO) == -1)
                {
                    _exit(1);
                }
                if (close(last_input) == -1)
                {
                    _exit(1);
                }
            }
        
        char **simple_tokens;
        char **exec_tokens;     // simple tokens with redirects thrown away

        //indicies for redirect tokens, if found
        int redirects[2] = {NO_REDIRECT, NO_REDIRECT};

        //size of simple tokens
        size_t smp_size = tokenize(pipe_tokens[tok_num], WHITESPACE, &simple_tokens);

        //if there are redirect characters to use, 
        if(find_redirects(simple_tokens, smp_size, redirects))
        {
            exec_tokens = malloc((smp_size + 1) * sizeof(char *));
            remove_redirects(&simple_tokens, &exec_tokens, smp_size, redirects[0], redirects[1]);

            //open input redirect, if the character is at a valid position
            if(redirects[0] != NO_REDIRECT && redirects[0] < (smp_size - 1))
            {
                char* in_string = simple_tokens[redirects[0]+1];
                
                int input_reader = open(in_string, O_RDONLY, 0600);
                
                if (input_reader == -1)
                {
                    _exit(1);
                }
                if(dup2(input_reader, STDIN_FILENO) == -1)
                {
                    _exit(1);
                }
                if(close(input_reader) == -1)
                {
                    _exit(1);
                }
            }
            
            //open output redirect, if the character is at a valid position
            if(redirects[1] != NO_REDIRECT && redirects[0] < smp_size -1)
            {
                char *out_string = simple_tokens[redirects[1]+1];

                int output_reader = open(out_string, O_WRONLY | O_CREAT, 0600);
                
                if (output_reader == -1)
                {
                    _exit(1);
                }
                if(dup2(output_reader, STDOUT_FILENO) == -1)
                {
                    _exit(1);
                }
                if(close(output_reader) == -1)
                {
                    _exit(1);
                }
            }
        
            free(simple_tokens);
            simple_tokens = NULL;
        }
        else //there are no redirect characters, just use simple_tokens
        {
            exec_tokens = simple_tokens;
        }
        
        if (execvp(exec_tokens[0], exec_tokens) == -1)
        {
            perror("execvp");
            //exec tokens is either simple tokens, or simple tokens has
            //already been cleaned up
            free(exec_tokens);
            exec_tokens = NULL;
            _exit(1);
        }
            
            break;
            
        default:
            break;
        }
        
        if (last_input != STDIN_FILENO)
        {
            if (close(last_input) == -1)
            {
                _exit(1);
            }
        }
        
        last_input = pfd[PIPE_READ_END];
        
        if (pfd[PIPE_WRITE_END] != STDOUT_FILENO)
        {
            if (close(pfd[PIPE_WRITE_END]) == -1)
            {
                _exit(1);
            }
        }
        
    } // our massive for loop
    
    /*
     * We don't need to close last_input here because in the for loop,
     * the last token never creates a pipe.
     */
}

int find_redirects(char ** ptr_to_tokens, int num_tokens, int *redirects) 
{
    int found = 0;
    for(int i = 0; i < num_tokens; ++i)
    {
        if(ptr_to_tokens[i][0] == '<'){
            redirects[0] = i;
            found++;
        }else if(ptr_to_tokens[i][0] == '>'){
            redirects[1] = i;
            found++;
        }
    }
    return found;
}

void remove_redirects(char *** ptr_tokens_old, char *** ptr_tokens_new, int num_tokens, int in_i, int out_i)
{
    int i, j = 0;
    for(i = 0; i < num_tokens; i++)
    {
        if(i != in_i && i != out_i && i != (in_i+1) && i != (out_i+1)){
            (*ptr_tokens_new)[j++] = (*ptr_tokens_old)[i];
        }

    }
    (*ptr_tokens_new)[j++] = NULL;
}


