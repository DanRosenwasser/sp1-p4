
/**
 * @author Daniel Rosenwasser
 * @author Matthew Newton
 */

#include "tokenizer.h"

#include <stdlib.h>
#include <string.h>

size_t tokenize(char* str, const char* delims, char*** pTokens)
{

    //
    // The only way to maximize the number of tokens you
    // have is to make them all as short as possible. The shortest a token
    // can be is 1 character. Thus, enough space must be allocated for
    // every other token.
    //
    // For instance, 3 characters yields the potential for 2 tokens at most
    // as seen with "t t".
    //
    // We then add 1 to the max_size for the trailing NULL
    //
    // This is just the easiest way to do this. 
    
    int max_size = 2+strlen(str)/2;
    
    (*pTokens) = malloc( max_size * sizeof(char*) ); 
    size_t num_tokens;  
    char* tok = strtok(str, delims);
    
    for (num_tokens = 0 ; tok != NULL; num_tokens++)
    {
        (*pTokens)[num_tokens] = tok;
        
        tok = strtok(NULL, delims);
    }
    
    // NULL-terminate
    (*pTokens)[num_tokens] = NULL;
    *pTokens = realloc(*pTokens, (num_tokens+1) * sizeof(char*));

    return num_tokens;
}


