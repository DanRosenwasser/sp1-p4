
/**
 * @author Daniel Rosenwasser
 * @author Matthew Newton
 */

#ifndef _EXEC_PIPE_CHAIN_H
#define _EXEC_PIPE_CHAIN_H

#define PIPE_SEPARATOR      "|"
#define WHITESPACE          " \n\t\r\f\v"

#define PIPE_READ_END     0
#define PIPE_WRITE_END    1

#include <stddef.h>

/**
 * Takes an array of pipe-delimited tokens and executes them in sequential
 * order, with stdin as the initial input stream and stdout as the final
 * output stream. This behavior only changes if input/output redirects are
 * present anywhere.
 *
 * @param pipe_tokens an array pipe-delimited tokens with a trailing NULL
 *
 */
void exec_pipe_chain(char** pipe_tokens);

#endif
